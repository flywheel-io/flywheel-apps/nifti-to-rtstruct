"""Parser module to parse gear config.json."""
from typing import Tuple

from flywheel_gear_toolkit import GearToolkitContext
from pathlib import Path


# This function mainly parses gear_context's config.json file and returns relevant inputs and options.
def parse_config(
    gear_context: GearToolkitContext,
) -> Tuple[Path, Path, float]:
    """Parse configuration from gear_context."""

    ref = Path(gear_context.get_input_path("dicom"))
    mask = Path(gear_context.get_input_path("nifti-mask"))
    threshold = float(gear_context.config.get('threshold'))

    return ref, mask, threshold
