"""Main module."""

import logging
import shutil
import zipfile
from pathlib import Path

import nibabel
import numpy as np
from rt_utils import RTStructBuilder


log = logging.getLogger(__name__)


def run(ref: Path, mask: Path, threshold: float, work_dir: Path, output_dir: Path):
    """Convert nifti to rt mask."""
    if zipfile.is_zipfile(ref):
        log.info(f"Unzipping {ref} to {work_dir}")
        with zipfile.ZipFile(ref, "r") as zipf:
            zipf.extractall(work_dir)
    else:
        log.info(f"Copying {ref} to {work_dir}")
        shutil.copy(ref, work_dir)

    log.info("Loading dicom series with RTStructBuilder")
    rtstruct = RTStructBuilder.create_new(dicom_series_path=str(work_dir))

    log.info("Loading nifti image")
    nifti = nibabel.load(mask)
    data = nifti.get_fdata()
    log.info("Scaling data to [0, 1]")
    d_min = np.min(data)
    d_max = np.max(data)
    data = (data - d_min) / (d_max - d_min)
    log.info("Applying threshold {threshold}")
    data[data >= threshold] = True
    data[data < threshold] = False

    log.info("Reorienting NIfTI data to DICOM")

    # NIFTI stores columns bottom to top, dicom top to bottom
    data = np.flip(data, 1)
    # Unsure exactly why I need this...
    data = np.swapaxes(data, 0, 1)
    log.info("Adding ROI as RTSTRUCT series")
    rtstruct.add_roi(mask=data.astype(bool))

    f_name = mask.name.split(".nii")[0] + "_RTSTRUCT.dcm"
    log.info(f"Saving output to {str(output_dir / f_name)}")
    rtstruct.save(str(output_dir / f_name))
    return output_dir / f_name
