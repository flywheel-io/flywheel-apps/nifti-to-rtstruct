from rt_utils import RTStructBuilder
import sys
import nibabel
import numpy as np


rtstruct = RTStructBuilder.create_new(dicom_series_path=sys.argv[1])

nifti = nibabel.load(sys.argv[2])


data = nifti.get_fdata()
data[data > 0.5] = 1
data[data < 0.5] = 0

# NIFTI stores columns bottom to top, dicom top to bottom
data = np.flip(data, 1)
# Unsure exactly why I need this...
data = np.swapaxes(data, 0, 1)

rtstruct.add_roi(mask=data.astype(bool))

rtstruct.save('/tmp/dicom')
