FROM flywheel/python-gdcm:master.3a9476be

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

RUN apt-get update && \
    apt-get install ffmpeg libsm6 libxext6 --no-install-recommends  -y

# Installing the current project (most likely to change, above layer can be cached)
# Note: poetry requires a README.md to install the current project
COPY run.py manifest.json README.md pyproject.toml poetry.lock $FLYWHEEL/
COPY fw_gear_nifti_to_rtstruct $FLYWHEEL/fw_gear_nifti_to_rtstruct
RUN poetry install --no-dev

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["poetry","run","python","/flywheel/v0/run.py"]

