# Nifti to RT-STRUCT

## Summary

A simple gear to convert a NIfTI mask to a DICOM RTSTRUCT, converting float values to
boolean.

## Usage

TODO

### Inputs

* __dicom__: Original DICOM to create RTSTRUCT for.
* __nifti-mask__: NIfTI segmentation mask to convert to RTSTRUCT

### Configuration

* __debug__ (boolean, default False): Include debug statements in output.
* __threshold__ (float, default 0.5): Threshold to use for binarizing input mask.

## Contributing

For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).
