#!/usr/bin/env python
"""The run script"""
import logging
import sys

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_nifti_to_rtstruct.main import run
from fw_gear_nifti_to_rtstruct.parser import parse_config

log = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> None:  # pragma: no cover
    """Parse config and run."""
    ref, mask, threshold = parse_config(context)
    saved_file = run(ref, mask, threshold, context.work_dir, context.output_dir)
    context.update_file_metadata(
        saved_file.name, modality="RTSTRUCT", type="dicom"
    )


# Only execute if file is run as main, not when imported by another module
if __name__ == "__main__":  # pragma: no cover
    # Get access to gear config, inputs, and sdk client if enabled.
    with GearToolkitContext() as gear_context:

        # Initialize logging, set logging level based on `debug` configuration
        # key in gear config.
        gear_context.init_logging()

        # Pass the gear context into main function defined above.
        main(gear_context)
